import re
import pandas as pd
from cims_app_1.models import *

def xltodf(filename,sheetname):
    df = pd.read_excel(filename,sheet_name=sheetname,engine='openpyxl')
    return df

def csvtodf(filename):
    df = pd.read_csv(filename)
    return df

def findfy(cdate):
    print(cdate)
    startfy = 0
    endfy = 0
    mm = cdate.month
    yy = cdate.year
    if mm >= 1 and mm <= 3:
        startfy = str(yy-1)
        endfy = str(yy)
    elif mm > 3 and mm <= 12:
        startfy = str(yy)
        endfy = str(yy + 1)
    finyr = startfy[2:] + '-' + endfy[2:]
    return finyr

def cal_wt(Unit,Packing,Quantity,weight_lst,df1_weight_lst):
    match = re.search('[0-9]+',Packing)
    Packing = int(match.group())
    if Unit.lower() == "katta":
        tot_wt = (Packing * Quantity)/100
    elif Unit.lower() == "bags":
        tot_wt = Quantity
    elif Unit.lower() == "ton":
        tot_wt = (Packing*Quantity)/1000
    weight_lst.append(tot_wt)
    df1_weight_lst.append(tot_wt)
    return tot_wt

def cal_amt(Unit,Weight,Rate,Quantity,amt_lst,df1_amt_lst):
    if Unit.lower() == "katta" or Unit.lower() == "ton":
        amt = (Weight * Rate)
    elif Unit.lower() == "bags":
        amt = (Quantity * Rate)
    amt_lst.append(amt)
    df1_amt_lst.append(amt)
    return amt

def cal_brokerage(wt,amt,sell_per,sell_rs,sell_brok_lst,df1_sell_brok_lst):
    match = re.search("[A-Za-z]+\s[A-Za-z]+",sell_per).group().lower()
    if match == "on weight":
        brok = wt/100 * sell_rs
    elif match == "on amount":
        brok = amt * (sell_rs/100)
    else:
        return 0
    sell_brok_lst.append(brok)
    df1_sell_brok_lst.append(brok)
    return brok

def unique(df,*args):
    unique_list = []
    for col in args:
        unique_list.extend(col.unique())
    unique_list = list(set(unique_list))
    return unique_list


def dict_generate(lst, tablemodel, colname, id_dict, tablenames):
    print(tablemodel)
    inner_dict = {}
    expr = f'tablemodel.get({colname} = i)'
    print(expr)
    current_table = tablemodel.model._meta.db_table

    for i in lst:
        try :
            oneentry = eval(expr)
            inner_dict[i] = oneentry
        except AccountTable.DoesNotExist:
            print(current_table)
            print(expr)
            print(f"Value {i} does NOT exists in Model {current_table} column {expr}")
    id_dict[current_table] = inner_dict
    tablenames.append(current_table)

