from django.contrib import admin
from .models import AccountInvoiceTable,AccountTable,CompanyTable,LKBrandTable,LKBSCodeTable,LKCurrencyTable,LKDeliveryTable,LKItemTable,LKPackingTable,LKPaymentModeTable,LKPercentageTable,LKStatusTable,LKTermsTable,LKTransportTable,LKUnitTable,LKUserTable,ContractTable,ContractSubItemsTable,ContractSettlementTable,ContractPaymentTable,ContractInvoiceTable,ContractInvoicePaymentTable,CompanyBankTable,AccountInvoiceSettlementTable

admin.site.register(AccountInvoiceTable)
admin.site.register(AccountTable)
admin.site.register(CompanyTable)
admin.site.register(LKBrandTable)
admin.site.register(LKBSCodeTable)
admin.site.register(LKCurrencyTable)
admin.site.register(LKDeliveryTable)
admin.site.register(LKItemTable)
admin.site.register(LKPackingTable)
admin.site.register(LKPaymentModeTable)
admin.site.register(LKPercentageTable)
admin.site.register(LKStatusTable)
admin.site.register(LKTermsTable)
admin.site.register(LKTransportTable)
admin.site.register(LKUnitTable)
admin.site.register(LKUserTable)
admin.site.register(ContractTable)
admin.site.register(ContractSubItemsTable)
admin.site.register(ContractSettlementTable)
admin.site.register(ContractPaymentTable)
admin.site.register(ContractInvoiceTable)
admin.site.register(ContractInvoicePaymentTable)
admin.site.register(CompanyBankTable)
admin.site.register(AccountInvoiceSettlementTable)

# Register your models here.
