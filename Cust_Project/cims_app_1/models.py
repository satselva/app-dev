from django.db import models

class CompanyTable(models.Model):
    CompanyID = models.AutoField(primary_key=True)
    CompanyName = models.CharField(max_length=50,null=True)
    Address = models.CharField(max_length=100,null=True)
    City = models.CharField(max_length=100,null=True)
    Pincode = models.CharField(max_length=50,null=True)
    Email = models.EmailField(max_length=100,null=True)
    Fax = models.CharField(max_length=50,null=True)
    PHOffice = models.CharField(max_length=50,null=True)
    PHResidence = models.CharField(max_length=50,null=True)
    Mobile = models.CharField(max_length=50,null=True)
    PAN = models.CharField(max_length=20,null=True)
    Caption = models.CharField(max_length=100,null=True)
    FinancialYearFrom = models.DateField(null=True)
    FinancialYearTo = models.DateField(null=True)
    class Meta:
        db_table = 'Company'

class CompanyBankTable(models.Model):
    BankID = models.AutoField(primary_key=True)
    CompanyID = models.ForeignKey(CompanyTable,on_delete=models.PROTECT,related_name='companyid_compbank',db_column='CompanyID')
    AccountName = models.CharField(max_length=200,null=True)
    AccountNumber = models.CharField(max_length=50,null=True)
    BankNameAndBranch = models.CharField(max_length=200,null=True)
    IFSC = models.CharField(max_length=50,null=True)
    AccountType = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'Company_Bank'

class LKBrandTable(models.Model):
    BrandID = models.AutoField(primary_key=True)
    BrandName = models.CharField(max_length=100,null=True)
    class Meta:
        db_table = 'lk_Brand'

class LKBSCodeTable(models.Model):
    BSCodeID = models.AutoField(primary_key=True)
    BSCodeName = models.CharField(max_length=100,null=True)
    class Meta:
        db_table = 'lk_BScode'

class LKCurrencyTable(models.Model):
    CurrencyID = models.AutoField(primary_key=True)
    CurrencyName = models.CharField(max_length=20,null=True)
    class Meta:
        db_table = 'lk_Currency'

class LKDeliveryTable(models.Model):
    DelID = models.AutoField(primary_key=True)
    DelName = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_Delivery'

class LKItemTable(models.Model):
    ItemID = models.AutoField(primary_key=True)
    ItemName = models.CharField(max_length=100,null=True)
    class Meta:
        db_table = 'lk_Item'

class LKPackingTable(models.Model):
    PackingID = models.AutoField(primary_key=True)
    PackingName = models.CharField(max_length=100,null=True)
    class Meta:
        db_table = 'lk_Packing'

class LKPaymentModeTable(models.Model):
    PayModeID = models.AutoField(primary_key=True)
    PayModeType = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_PaymentMode'

class LKPercentageTable(models.Model):
    PercentageID = models.AutoField(primary_key=True)
    PercentageName = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_Percentage'

class LKStatusTable(models.Model):
    StatusID = models.AutoField(primary_key=True)
    Status = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_Status'

class LKTermsTable(models.Model):
    ID = models.AutoField(primary_key=True)
    Terms = models.CharField(max_length=100,null=True)
    class Meta:
        db_table = 'lk_Terms'

class LKTransportTable(models.Model):
    TransportID = models.AutoField(primary_key=True)
    TransportName = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_Transport'

class LKUnitTable(models.Model):
    UnitID = models.AutoField(primary_key=True)
    UnitName = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_Unit'

class LKUserTable(models.Model):
    UserName = models.CharField(max_length=50,null=True)
    Password = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'lk_User'

class AccountTable(models.Model):
    AccountID = models.AutoField(primary_key=True)
    AccountName = models.CharField(max_length=500,null=True)
    BSCodeID = models.ForeignKey(LKBSCodeTable,on_delete=models.PROTECT,related_name='bscodeid',db_column='BSCodeID')
    Address = models.CharField(max_length=500,null=True)
    Email = models.EmailField(max_length=100,null=True)
    CompanyID = models.ForeignKey(CompanyTable,on_delete=models.PROTECT,related_name='companyid_acc',db_column='CompanyID')
    AccountOpeningBalance = models.FloatField(null=True)
    City = models.CharField(max_length=50,null=True)
    State = models.CharField(max_length=50,null=True)
    Pincode = models.CharField(max_length=50,null=True)
    Mobile = models.CharField(max_length=200,null=True)
    GST = models.CharField(max_length=50,null=True)
    class Meta:
        db_table = 'Account'

class AccountInvoiceTable(models.Model):
    InvoiceID = models.AutoField(primary_key=True)
    AccountID = models.ForeignKey(AccountTable,on_delete=models.PROTECT,related_name='account_name',db_column='AccountID')
    InvoiceNumber = models.CharField(max_length=50,null=True)
    AccountType = models.CharField(max_length=10,null=True)
    FY = models.CharField(max_length=50,null=True)
    PaymentSettled = models.BooleanField(null=True)
    CompanyID = models.ForeignKey(CompanyTable,on_delete=models.PROTECT,related_name='companyid_acc_invoice',db_column='CompanyID')
    InvoiceDate = models.DateField(null=True)
    InvoiceName = models.ForeignKey(CompanyTable,on_delete=models.PROTECT,related_name='invoice_name',db_column='InvoiceName')
    class Meta:
        db_table = 'Account_Invoice'

class AccountInvoiceSettlementTable(models.Model):
    SettlementID = models.AutoField(primary_key=True)
    InvoiceID = models.ForeignKey(AccountInvoiceTable,on_delete=models.PROTECT,related_name='invoice_name',db_column='InvoiceID')
    PaymentDate = models.DateField(null=True)
    PaymentMode = models.ForeignKey(LKPaymentModeTable,on_delete=models.PROTECT,related_name='paymentmode',db_column='PaymentMode')
    TransactionNo = models.CharField(max_length=100,null=True)
    Dated = models.DateField(null=True)
    AmountSettled = models.FloatField(null=True)
    Drawn = models.CharField(max_length=100,null=True)
    FromAccount = models.ForeignKey(CompanyBankTable,on_delete=models.PROTECT,related_name='fromaccount_name',db_column='FromAccount')
    Notes = models.CharField(max_length=500,null=True)
    class Meta:
        db_table = 'Account_Invoice_Settlement'

class ContractTable(models.Model):
    ContractID = models.AutoField(primary_key=True)
    ContractNumber = models.CharField(max_length=500,null=True)
    ContractDate = models.DateField(null=True)
    FY = models.CharField(max_length=50,null=True)
    #Seller = models.IntegerField(null=True)
    Seller = models.ForeignKey(AccountTable,on_delete=models.PROTECT,related_name='seller_name',db_column='Seller')
    SellerPercentageOn = models.ForeignKey(LKPercentageTable,on_delete=models.PROTECT,related_name='seller_percent',db_column='SellerPercentageOn')
    Buyer = models.ForeignKey(AccountTable,on_delete=models.PROTECT,related_name='buyer_name',db_column='Buyer')
    BuyerPercentageOn = models.ForeignKey(LKPercentageTable,on_delete=models.PROTECT,related_name='buyer_percent',db_column='BuyerPercentageOn')
    Broker = models.ForeignKey(AccountTable,on_delete=models.PROTECT,related_name='broker_name',db_column='Broker')
    BrokerPercentageOn = models.ForeignKey(LKPercentageTable,on_delete=models.PROTECT,related_name='broker_percent',db_column='BrokerPercentageOn')
    BuyerBrokerage = models.FloatField(null=True)
    SellerBrokerage = models.FloatField(null=True)
    BrokerBrokerage = models.FloatField(null=True)
    Freight = models.FloatField(null=True)
    DeliveryUpto = models.CharField(max_length=500,null=True)
    Terms = models.ForeignKey(LKTermsTable,on_delete=models.PROTECT,related_name='terms',db_column='Terms')
    Status = models.ForeignKey(LKStatusTable,on_delete=models.PROTECT,related_name='status',db_column='Status')
    LoadedFrom = models.CharField(max_length=500,null=True)
    LoadedTo = models.CharField(max_length=500,null=True)
    Transport = models.ForeignKey(LKTransportTable,on_delete=models.PROTECT,related_name='transport',db_column='Transport')
    Delivery = models.ForeignKey(LKDeliveryTable,on_delete=models.PROTECT,related_name='delivery',db_column='Delivery')
    DueDate = models.DateField(null=True)
    LoadedOn = models.DateField(null=True)
    ArrivalDate = models.DateField(null=True)
    PaymentDueDate = models.DateField(null=True)
    CForm = models.BooleanField(null=True)
    Notes = models.CharField(max_length=500,null=True)
    BuyerDCB = models.BooleanField(null=True)
    BuyerRs = models.FloatField(null=True)
    SellerDCB = models.BooleanField(null=True)
    SellerRs = models.FloatField(null=True)
    BrokerDCB = models.BooleanField(null=True)
    BrokerRs = models.FloatField(null=True)
    SellerInvoiceID = models.ForeignKey(AccountInvoiceTable,null=True,on_delete=models.PROTECT,related_name='sellerinvoiceid',db_column='SellerInvoiceID')
    BuyerInvoiceID = models.ForeignKey(AccountInvoiceTable,null=True,on_delete=models.PROTECT,related_name='buyerinvoiceid',db_column='BuyerInvoiceID')
    SelectedCurrency = models.ForeignKey(LKCurrencyTable,on_delete=models.PROTECT,related_name='currency',db_column='SelectedCurrency')
    CompanyID = models.ForeignKey(CompanyTable,on_delete=models.PROTECT,related_name='companyid_contract',db_column='CompanyID')
    class Meta:
        db_table = 'Contract'

class ContractInvoiceTable(models.Model):
    InvoiceID = models.AutoField(primary_key=True)
    ContractID = models.ForeignKey(ContractTable,on_delete=models.PROTECT,related_name='contractid_contract_inv',db_column='ContractID')
    InvoiceDate = models.DateField(null=True)
    InvoiceNumber = models.CharField(max_length=50,null=True)
    AmountDue = models.FloatField(null=True)
    Freight = models.FloatField(null=True)
    TotalQty = models.FloatField(null=True)
    class Meta:
        db_table = 'Contract_Invoice'

class ContractInvoicePaymentTable(models.Model):
    InvoiceID = models.AutoField(primary_key=True)
    ContractID = models.ForeignKey(ContractTable,on_delete=models.PROTECT,related_name='contractid_contract_inv_pay',db_column='ContractID')
    InvoiceDate = models.DateField(null=True)
    InvoiceNumber = models.ForeignKey(ContractInvoiceTable,on_delete=models.PROTECT,related_name='invoice_name',db_column='InvoiceNumber')
    AmountDue = models.FloatField(null=True)
    Freight = models.FloatField(null=True)
    TotalQty = models.FloatField(null=True)
    Interest = models.FloatField(null=True)
    AdditionsFreight = models.FloatField(null=True)
    OtherAdditions = models.FloatField(null=True)
    TotalAdditions = models.FloatField(null=True)
    Discount = models.FloatField(null=True)
    WgtShortage = models.FloatField(null=True)
    ChqToBuyer = models.FloatField(null=True)
    DisputeRs = models.FloatField(null=True)
    DisputeQuintal = models.FloatField(null=True)
    DeductionsFreight = models.FloatField(null=True)
    BillExcess = models.FloatField(null=True)
    OtherDeductions = models.FloatField(null=True)
    TotalDeductions = models.FloatField(null=True)
    FinalAmtDue = models.FloatField(null=True)
    class Meta:
        db_table = 'Contract_Invoice_Payment'


class ContractPaymentTable(models.Model):
    PaymentID = models.AutoField(primary_key=True)
    ContractID = models.ForeignKey(ContractTable,on_delete=models.PROTECT,related_name='contractid_contractpay',db_column='ContractID')
    Interest = models.FloatField(null=True)
    AdditionsFreight = models.FloatField(null=True)
    OtherAdditions = models.FloatField(null=True)
    TotalAdditions = models.FloatField(null=True)
    Discount = models.FloatField(null=True)
    WgtShortage = models.FloatField(null=True)
    ChqToBuyer = models.FloatField(null=True)
    DisputeRs = models.FloatField(null=True)
    DisputeQuintal = models.FloatField(null=True)
    DeductionsFreight = models.FloatField(null=True)
    BillExcess = models.FloatField(null=True)
    OtherDeductions = models.FloatField(null=True)
    TotalDeductions = models.FloatField(null=True)
    FinalAmtDue = models.FloatField(null=True)
    PaymentSettled = models.BooleanField(null=True)
    class Meta:
        db_table = 'Contract_Payment'

class ContractSettlementTable(models.Model):
    SettlementID = models.AutoField(primary_key=True)
    ContractID = models.ForeignKey(ContractTable,on_delete=models.PROTECT,related_name='contractid_contract_settl',db_column='ContractID')
    PaymentDate = models.DateField(null=True)
    ChqNo = models.CharField(max_length=100,null=True)
    Dated = models.DateField(null=True)
    AmountSettled = models.FloatField(null=True)
    Drawn = models.CharField(max_length=100,null=True)
    Remarks = models.CharField(max_length=100,null=True)
    Notes = models.CharField(max_length=500,null=True)
    BalanceAmt = models.FloatField(null=True)
    class Meta:
        db_table = 'Contract_Settlement'

class ContractSubItemsTable(models.Model):
    ContractSubItemsID = models.AutoField(primary_key=True)
    ContractID = models.ForeignKey(ContractTable,on_delete=models.PROTECT,related_name='contractid_subitem',db_column='ContractID')
    ItemName = models.CharField(max_length=100,null=True)
    BrandName = models.CharField(max_length=100,null=True)
    Packing = models.CharField(max_length=100,null=True)
    Quantity = models.FloatField(null=True)
    Unit = models.CharField(max_length=100,null=True)
    Weight = models.FloatField(null=True)
    Rate = models.FloatField(null=True)
    Amount = models.FloatField(null=True)
    SellerBrokerage = models.FloatField(null=True)
    BuyerBrokerage = models.FloatField(null=True)
    BrokerBrokerage = models.FloatField(null=True)
    class Meta:
        db_table = 'Contract_SubItems'
# Create your models here.
