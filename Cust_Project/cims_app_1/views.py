from django.shortcuts import render
from django.http import HttpResponse
from cims_app_1.models import *
from cims_app_1.qry import *
from Cust_Project.settings import STATIC_DIR
import os
import re


def index(request):

    filename = os.path.join(STATIC_DIR,'contract.xlsx')
    contract_df = xltodf(filename,'Contract')
    subitem_df = xltodf(filename,'Item')

    filename1 = os.path.join(STATIC_DIR,'contract.csv')
    filename2 = os.path.join(STATIC_DIR,'item.csv')
    #contract_df = csvtodf(filename1)
    #subitem_df = csvtodf(filename2)

    overall  = unique(contract_df,contract_df.Buyer,
               contract_df.Seller,contract_df.Broker)

    overall_percent = unique(contract_df,contract_df.BuyerPercentageOn,
                      contract_df.SellerPercentageOn,
                      contract_df.BrokerPercentageOn)

    #Read the DF and create a unique list of each column
    #We will do the id to name mapping later

    term = unique(contract_df,contract_df.Terms)
    stat = unique(contract_df,contract_df.Status)
    trans = unique(contract_df,contract_df.Transport)
    delv = unique(contract_df,contract_df.Delivery)
    crncy = unique(contract_df,contract_df.SelectedCurrency)
    comp = unique(contract_df,contract_df.CompanyID)

    odict = {}
    tablenames = []

    overall_ulist = [overall, overall_percent, term, stat,
                    trans, delv, crncy, comp]

    tablemodellist = [AccountTable.objects,
                      LKPercentageTable.objects,
                      LKTermsTable.objects,
                      LKStatusTable.objects,
                      LKTransportTable.objects,
                      LKDeliveryTable.objects,
                      LKCurrencyTable.objects,
                      CompanyTable.objects]

    tablecollist = ['AccountName', 'PercentageName', 'Terms', 'Status',
                   'TransportName', 'DelName', 'CurrencyName', 'CompanyName']

    #This will create the id to table mapping for the data present in the contract dataframe

    for ulist,tablemodel,tablecol in zip(overall_ulist, tablemodellist, tablecollist):
        dict_generate(ulist, tablemodel, tablecol, odict, tablenames)


    df1_weight_lst = []
    df1_amt_lst = []
    df1_sell_brok_lst = []
    df1_buy_brok_lst = []
    df1_brok_brok_lst = []

    for i in contract_df.index:

        #Below lists are initilized to update the per_contract_items_df
        weight_lst = []
        amt_lst = []
        sell_brok_lst = []
        buy_brok_lst = []
        brok_brok_lst = []

        sell_rs = contract_df['SellerRs'][i]
        sell_per = contract_df['SellerPercentageOn'][i]
        buy_rs = contract_df['BuyerRs'][i]
        buy_per = contract_df['BuyerPercentageOn'][i]
        brok_rs = contract_df['BrokerRs'][i]
        brok_per = contract_df['BrokerPercentageOn'][i]

        contract_date = contract_df['ContractDate'][i]
        fy = findfy(contract_date)
        contract_num = contract_df['ContractNumber'][i]
        contract_date = contract_df['ContractDate'][i]
        seller_id = odict[tablenames[0]][contract_df['Seller'][i]]
        s_percent_id = odict[tablenames[1]][contract_df['SellerPercentageOn'][i]]
        buyer_id = odict[tablenames[0]][contract_df['Buyer'][i]]
        b_percent_id = odict[tablenames[1]][contract_df['BuyerPercentageOn'][i]]
        broker_id = odict[tablenames[0]][contract_df['Broker'][i]]
        bro_percent_id = odict[tablenames[1]][contract_df['BrokerPercentageOn'][i]]
        freight = contract_df['Freight'][i]
        delivery_upto =  contract_df['DeliveryUpto'][i]
        terms = odict[tablenames[2]][contract_df['Terms'][i]]
        status = odict[tablenames[3]][contract_df['Status'][i]]
        loaded_from = contract_df['LoadedFrom'][i]
        loaded_to = contract_df['LoadedTo'][i]
        transport = odict[tablenames[4]][contract_df['Transport'][i]]
        delivery = odict[tablenames[5]][contract_df['Delivery'][i]]
        due_date = contract_df['DueDate'][i]
        payment_due_date = contract_df['PaymentDueDate'][i]
        notes = contract_df['Notes'][i]
        buyer_dcb = contract_df['BuyerDCB'][i]
        buyer_rs = contract_df['BuyerRs'][i]
        seller_dcb = contract_df['SellerDCB'][i]
        seller_rs = contract_df['SellerRs'][i]
        broker_dcb = contract_df['BrokerDCB'][i]
        broker_rs = contract_df['BrokerRs'][i]
        selected_currency = odict[tablenames[6]][contract_df['SelectedCurrency'][i]]
        company_id = odict[tablenames[7]][contract_df['CompanyID'][i]]

        print(delivery)

        #Doing an insert operation in the DB
        b = ContractTable(
                ContractNumber = contract_num,
                ContractDate = contract_date,
                FY = fy,
                Seller = seller_id,
                SellerPercentageOn = s_percent_id,
                Buyer = buyer_id,
                BuyerPercentageOn = b_percent_id,
                Broker = broker_id,
                BrokerPercentageOn = bro_percent_id,
                Freight = freight,
                DeliveryUpto = delivery_upto,
                Terms = terms,
                Status = status,
                LoadedFrom = loaded_from,
                LoadedTo = loaded_to,
                Transport = transport,
                Delivery = delivery,
                DueDate = due_date,
                PaymentDueDate = payment_due_date,
                Notes = notes,
                BuyerDCB = buyer_dcb,
                BuyerRs = buyer_rs,
                SellerDCB = seller_dcb,
                SellerRs = seller_rs,
                BrokerDCB = broker_dcb,
                BrokerRs = broker_rs,
                SelectedCurrency = selected_currency,
                CompanyID = company_id
                )

        retval = b.save()

        #Getting the contract id of the b.save() contract
        obj = ContractTable.objects.latest('ContractID')

        #With the contract being inserted we shall now insert the subitems associated with the contract
        #in the subitems table

        per_contract_items_df = subitem_df[(subitem_df==contract_df['ContractNumber'][i]).any(axis=1)]

        for row in per_contract_items_df.iterrows():
                wt = cal_wt(row[1]['Unit'],row[1]['Packing'],
                            row[1]['Quantity'],weight_lst,df1_weight_lst)

                amt = cal_amt(row[1]['Unit'],wt,row[1]['Rate'],
                              row[1]['Quantity'],amt_lst,df1_amt_lst)
                sell_brok = cal_brokerage(wt,amt,sell_per,sell_rs,
                            sell_brok_lst,df1_sell_brok_lst)

                buy_brok = cal_brokerage(wt,amt,buy_per,buy_rs,
                           buy_brok_lst,df1_buy_brok_lst)

                brok_brok = cal_brokerage(wt,amt,brok_per,brok_rs,
                            brok_brok_lst,df1_brok_brok_lst)

                c = ContractSubItemsTable(
                       ContractID = obj,
                       ItemName =  row[1]['ItemName'],
                       BrandName = row[1]['BrandName'],
                       Packing = row[1]['Packing'],
                       Quantity = row[1]['Quantity'],
                       Unit = row[1]['Unit'],
                       Rate = row[1]['Rate'],
                       Weight = wt,
                       Amount = amt,
                       SellerBrokerage = sell_brok,
                       BuyerBrokerage = buy_brok,
                       BrokerBrokerage = brok_brok
                       )
                c.save()

        per_contract_items_df['Weight'] = weight_lst
        per_contract_items_df['Amount'] = amt_lst
        per_contract_items_df['SellerBrokerage'] = sell_brok_lst
        per_contract_items_df['BuyerBrokerage'] = buy_brok_lst
        per_contract_items_df['BrokerBrokerage'] = brok_brok_lst

        #Now we update the Contract Table after the subitems have been inserted successfully
        b.SellerBrokerage = per_contract_items_df['SellerBrokerage'].sum()
        b.BuyerBrokerage = per_contract_items_df['BuyerBrokerage'].sum()
        b.BrokerBrokerage = per_contract_items_df['BrokerBrokerage'].sum()
        b.save()

    subitem_df['Weight'] = df1_weight_lst
    subitem_df['Amount'] = df1_amt_lst
    subitem_df['SellerBrokerage'] = df1_sell_brok_lst
    subitem_df['BuyerBrokerage'] = df1_buy_brok_lst
    subitem_df['BrokerBrokerage'] = df1_brok_brok_lst



        #except:
        #    print("fail with exception")
    return HttpResponse("Hello,world.You are at the polls index")

# Create your views here.
